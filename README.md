# Crafting web applications

Article: https://danbunea.blogspot.com/2019/09/how-to-solve-it-crafting-web-apps-part-i.html

Presentation link: #todo

Demo link: http://www.danbunea.ro/presentations/todos/


## Overview

When you have to develop a new web application, how do you approach it?

If you had to develop this:

![](https://j.gifs.com/K1mnRG.gif)

What would be the first step?

### Crafting web applications


THEORY - How should you start with a web app::

#### Strategy

##### Goals::

* Break the complexity down (state machines, separation of concerns)
* Build robustness into the system (atomicity with rollback)


*How?*

##### Steps::

1. Finite state machine diagram (state/transitions starting from the UI)
2. States (domain modelling for the states)
3. Transitions (TDD the controller)
4. Presentation/UI for the states (TDD the view)

#### Tactics

##### Patterns::

* finite state machines
* separation of concerns using MVC
* atomicity = all or nothing

*Yes, but how?*

##### Techniques::

* TDD - for the non UI
* TDD - for the UI
* Design by contract


## Development

To get an interactive development environment run:

    lein fig:build

This will auto compile and send all changes to the browser without the
need to reload. After the compilation process is complete, you will
get a Browser Connected REPL. An easy way to try it is:

    (js/alert "Am I connected?")

and you should see an alert in the browser window.

To clean all compiled files:

	lein clean

To create a production build run:

	lein clean
	lein fig:min

## TDD

Testing

    lein fig:test

or while developing:

    http://localhost:9500/figwheel-extra-main/auto-testing

## License

Copyright © 2018 FIXME

Distributed under the Eclipse Public License either version 1.0 or (at your option) any later version.
