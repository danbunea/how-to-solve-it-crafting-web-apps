(ns tech.danbunea.safe-macro-should
  (:require
    [tech.danbunea.pi :refer [commit!]]
    [cljs.test :refer-macros [deftest is testing use-fixtures]]
    )
  (:require-macros
    [tech.danbunea.macros :refer [safe]]
    ))

(def data-model (atom {:value 0}))


(defn step-2 [state]
  (update state :value inc))


(defn atomic-operation! []
  (safe
    (-> @data-model
        (assoc :value 1)
        step-2
        (commit! data-model))
    ))



(deftest atomic-operations
  (let [initial-value {:value 0}]
    (testing "all goes well"
      ;given
      (reset! data-model initial-value)
      ;when
      (atomic-operation!)
      ;then
      (is (= {:value 2} @data-model)))
    (testing "step-2 fails"
      ;given
      (reset! data-model initial-value)
      (with-redefs [step-2 (fn [_ _] (throw (ex-info "The ice cream has melted!" {})))]
                   ;when
                   (atomic-operation!)
                   ;then
                   (is (= initial-value @data-model)))
      )))