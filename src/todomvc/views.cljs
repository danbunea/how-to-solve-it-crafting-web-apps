(ns todomvc.views
  (:require [todomvc.controller :as controller]
            [reagent.core :as r]
            ))

(def ENTER 13)
(def ESCAPE 27)



(defn text-input-component [text class-name on-changed on-quit]
  (let [inner-state (r/atom {:text text})]
    (fn render [text class-name on-changed on-quit]
      [:input {:class       class-name
               :type        "text"
               :value       (:text @inner-state)
               :placeholder "What needs to be done?"
               :on-change   #(swap! inner-state assoc :text (-> % .-target .-value))
               :on-key-down #(condp = (.-which %)
                               ENTER (when-not (empty? (:text @inner-state))
                                           (let [text (:text @inner-state)]
                                             (swap! inner-state assoc :text "")
                                             (on-changed text)))
                               ESCAPE (do
                                            (swap! inner-state assoc :text "")
                                            (on-quit))
                               nil)}])))



(defn todo-input-component []
  [text-input-component "" "new-todo" controller/save! controller/list!])




(defn todo-item-component [todo editing?]
  [:li.todo {:class (str (if (:done? todo) "completed " "")
                         (if editing? "editing " ""))}
   [:div.view
    [:input.toggle {:id        (str "checkbox_" (:id todo))
                    :type      "checkbox"
                    :checked   (:done? todo)
                    :on-change #(controller/toggle! (:id todo))
                    }]
    [:label {
             :id              (str "label_" (:id todo))
             :on-double-click #(controller/edit! (:id todo))
             }
     (:text todo)]
    [:button.destroy {
                      :id       (str "button_" (:id todo))
                      :on-click #(controller/delete! (:id todo))
                      }]]
   (when editing?
     [text-input-component
      (:text todo)
      "edit"
      #(controller/save! (:id todo) % (:done? todo) (:time todo))
      controller/list!
      ])

   ]
  )



(defn todos-list-component [state]
  (let [filter-fn (cond
                    (true? (-> state :context :filter)) #(filter (fn [item] (false? (:done? item))) %)
                    (false? (-> state :context :filter)) #(filter (fn [item] (true? (:done? item))) %)
                    :else identity)

        todos (->> state
                   :todos
                   vals
                   (sort-by :time)
                   filter-fn)]
    [:ul.todo-list
     (for [todo todos]
       ^{:key (:id todo)}
       [todo-item-component todo (and (= "edit" (get-in state [:context :status]))
                                      (= (:id todo) (get-in state [:context :selected-id])))]
       )]))




(defn todos-count-component [state]
  (let [active-count (->> state
                          :todos
                          vals
                          (filter #(not (:done? %)))
                          count)]
    [:span.todo-count
     [:strong (str active-count (if (= 1 active-count) " item " " items ") "left")
      ]]))



(defn todos-filters-component [filter]
  [:ul.filters
   [:li [:a#all {:class (if (nil? filter) "selected" "") :on-click #(controller/filter! nil)} "All"]]
   [:li [:a#active {:class (if (true? filter) "selected" "") :on-click #(controller/filter! true)} "Active"]]
   [:li [:a#completed {:class (if (false? filter) "selected" "") :on-click #(controller/filter! false)} "Completed"]]]
  )



(defn screen-component [state]
  [:div
   [:section.todoapp
    [:header.header
     [:h1 "todos"]
     [todo-input-component]]
    (when-not (empty? (:todos state))
      [:section.main
       [todos-list-component state]])
    (when-not (empty? (:todos state))
      [:footer.footer
       [todos-count-component state]
       [todos-filters-component (-> state :context :filter)]
       ])
    ]
   [:footer.info
    [:p "Double-click to edit a todo"]
    [:p "Credits: "
     [:a {:href "https://twitter.com/danbunea"} "Dan Bunea"]]
    [:p "Part of " [:a {:href "http://todomvc.com"} "TodoMVC"]]]])